### Klausur / Test am 20.5.19

## Aufgabe 1

Fehler finden in der server.js und oder einer *.ejs-Datei

Dazu den Nodemon starten und auf die Meldungen achten.

Nur die Schriftlichen Ausarbeitungen auf Papier sind Gegenstand der Leistungsbewertung.

## Aufgabe 2

* Klassendefinition
* Deklaration (Bekanntmachung)
* Instanziieurung (Bereitstellung von Arbeitsspeicher; erkennt man immer am reservierten Wort 'new')
* Initialisierung 

## Aufgabe 3

 // Deklaration:
let fruehling 

// Instanziierung:
fruehling = new Jahreszeit()

// Alternativ können Deklaration und Instanziierung in einer Zeile geschrieben werden:
let fruehling = new Jahreszeit()
let sommer = new Jahreszeit()

## Aufgabe 4

Code-Schnipsel mit einem Satz erklären.
